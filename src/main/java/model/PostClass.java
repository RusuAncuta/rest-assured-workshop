package model;

public class PostClass {
    private int id;
    private int userid;
    private String title;
    private String body;

    public PostClass(){};

    public PostClass(int id, int userid, String title, String body){
        this.id =id;
        this.userid=userid;
        this.title=title;
        this.body=body;
    }

    public int getid() {
        return id;
    }

    public int getUserid() {
        return userid;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public void setid(int id) {
        id = id;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
