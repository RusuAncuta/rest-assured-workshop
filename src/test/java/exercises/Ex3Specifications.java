package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Ex3Specifications {
    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    /*************************************************************
     * Create request and response specifications using builders
     * Request specification should include our base URI and
     * a base path that points to comments collection
     * Response specification should expect status code 200
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost:3000");
        builder.setBasePath("/comments");
        requestSpec = builder.build();

        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();
    }



    /**********************************************************
     * Delete the comment with ID 1 (use both specifications)
     *********************************************************/
    @Test
    public void testDeleteComment() {
        given().log().all().spec(requestSpec).
                pathParam("id",1).when().delete("/{id}");
    }

    /**********************************************************
     * Update the comment with ID 2
     * (use both specifications)
     *********************************************************/
    @Test
    public void testAddComment() {
    given().log().all().spec(requestSpec).pathParam("id",2).body("{\n" +
            "    \"postId\": 1,\n" +
            "    \"id\": 2,\n" +
            "    \"name\": \"quo vero reiciendis velit similique earum\",\n" +
            "    \"email\": \"Jayne_Kuhic@sydney.com\",\n" +
            "    \"body\": \"my comment\"\n" + //modific doar componenta dorita, restul raman la fel
            "  }")
            .header("content-type","application/json").when().put("/{id}").then().spec(responseSpec);


    }
}