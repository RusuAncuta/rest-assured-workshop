package exercises;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

public class Ex1Basic {

    @BeforeAll
    public static void setUp(){
        //we set the base URI so that we don't have to use the get(".../users").
        // this way we'll use get() simple without any other param
        RestAssured.baseURI="http://localhost:3000";
        RestAssured.port=3000;
        RestAssured.basePath="/users";
    }
    /*******************************************************
     * Send a GET request to "  http://localhost:3000/users"
     * and check that the response has HTTP status code 200
     ******************************************************/
    @Test
    public void testGetUsers() {
    given().when().get().then().assertThat().statusCode(200);
    }


    /*******************************************************
     * Send a GET request to ""  http://localhost:3000/incorrect"
     * and check that the answer has HTTP status code 404
     ******************************************************/
    @Test
    public void testIncorrectRequest() {

    given().when().get("http://localhost:3000/incorrect").then().assertThat().statusCode(404);
    }


    /*******************************************************
     * Retrieve users collection and assert that
     * the returned content type is "application/json"
     * and the collection has 10 items
     ******************************************************/
    @Test
    public void testUsersSize() {
    given().when().get().then().assertThat().contentType("application/json")
            .and().body("size()",equalTo(10));
    }


    /*******************************************************
     * Check that the users collection contains items
     * having company name "Johns Group"
     ******************************************************/
    @Test
    public void testUsersWithCompany() {
    given().when().get().then().assertThat().body("company.name",hasItem("Johns Group"));
    }

}
