package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import model.PostClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Ex5ObjectModeling {
    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;
    /*************************************************************
     * Request specification should include our base URI
     * and base path pointing to posts collection
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost:3000");
        builder.setBasePath("/posts");
        requestSpec = builder.build();

        responseSpec = new ResponseSpecBuilder().expectStatusCode(201).build();
    }

    /**********************************************************
     * Check the title of any post using deserialization.
     * You should create a new class Post.java that has the same
     * structure as the objects in the posts collection and
     * has a getter method for the title property.
     *********************************************************/
    @Test
    public void testGetPost() {
        PostClass post=given().spec(requestSpec).pathParam("id",1).when().get("/{id}").as(PostClass.class);

        assertEquals("sunt aut facere repellat provident occaecati excepturi optio reprehenderit",post.getTitle());
        //assertEquals();


    }

    /**********************************************************
     * Add a new item to posts collection using serialization.
     * You should use the new class Post.java - create a new
     * instance of it and set it as the request body.
     * Make sure your Post.java class has a constructor.
     * Check that the response status code is 201 (created).
     *********************************************************/
    @Test
    public void testAddPost() {
    PostClass newPost= new PostClass(121,1,"this is my title","some more text just to fill this entry");
    given().log().all().spec(requestSpec).header("content-type","application/json")
            .body(newPost).when()
            .post("http://localhost:3000/posts").then().spec(responseSpec);
    }
}
