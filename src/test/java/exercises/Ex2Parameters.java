package exercises;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class Ex2Parameters {

    @BeforeAll
    public static void setUp() {
        //we set the base URI so that we don't have to use the get(".../users").
        // this way we'll use get() simple without any other param
        RestAssured.baseURI = "http://localhost:3000";
        RestAssured.port = 3000;
        //RestAssured.basePath = "/todos";
    }

    /*******************************************************
     * Check that the user with ID 1 has 15 todos
     * Use pathParam for creating the request
     ******************************************************/
    @Test
    public void testTodosForUser() {
        given().log().all().queryParam("userId", 1)
                .when()
                .get("/todos")
                .then()
                .log().all()
                .assertThat()
                .body("size()", equalTo(20));
    }


    /*******************************************************
     * Check that the user with ID 1 has a post called "qui est esse"
     * Use queryParam for creating the request
     ******************************************************/
    @Test
    public void testPostForUser() {

    }
}